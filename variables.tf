variable "project_environment" {
  type = string
}

variable "argocd_namespace" {
  type    = string
  default = "argocd"
}
variable "argocd_project" {
  type    = string
  default = "default"
}

variable "filebeat_taints" {
  type = list(object({
    key    = string
    value  = string
    effect = string
  }))
  default = []
}
variable "filebeat_namespace" {
  type    = string
  default = "beats"
}
variable "filebeat_repo_url" {
  type = string
}
variable "filebeat_lop_address" {
  type    = string
  default = ""
}
variable "filebeat_lop_secret_name" {
  type    = string
  default = "filebeat-lop-crt"
}
variable "filebeat_ca_crt" {
  type      = string
  sensitive = true
}
variable "filebeat_key" {
  type      = string
  sensitive = true
}
variable "filebeat_crt" {
  type      = string
  sensitive = true
}
variable "filebeat_log_paths" {
  type = list(object({
    name    = string
    type    = string
    service = optional(string)
  }))
  default = []
}
