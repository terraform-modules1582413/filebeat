<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | ~> 2.9.0 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | ~> 2.18.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | ~> 2.18.1 |
| <a name="provider_time"></a> [time](#provider\_time) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [kubernetes_manifest.argocd_application](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/manifest) | resource |
| [kubernetes_secret.repository](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [time_sleep.wait_10_seconds](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/sleep) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_argocd_namespace"></a> [argocd\_namespace](#input\_argocd\_namespace) | n/a | `string` | `"argocd"` | no |
| <a name="input_argocd_project"></a> [argocd\_project](#input\_argocd\_project) | n/a | `string` | `"default"` | no |
| <a name="input_filebeat_ca_crt"></a> [filebeat\_ca\_crt](#input\_filebeat\_ca\_crt) | n/a | `string` | n/a | yes |
| <a name="input_filebeat_crt"></a> [filebeat\_crt](#input\_filebeat\_crt) | n/a | `string` | n/a | yes |
| <a name="input_filebeat_key"></a> [filebeat\_key](#input\_filebeat\_key) | n/a | `string` | n/a | yes |
| <a name="input_filebeat_log_paths"></a> [filebeat\_log\_paths](#input\_filebeat\_log\_paths) | n/a | <pre>list(object({<br>    name    = string<br>    type    = string<br>    service = optional(string)<br>  }))</pre> | `[]` | no |
| <a name="input_filebeat_lop_address"></a> [filebeat\_lop\_address](#input\_filebeat\_lop\_address) | n/a | `string` | `""` | no |
| <a name="input_filebeat_lop_secret_name"></a> [filebeat\_lop\_secret\_name](#input\_filebeat\_lop\_secret\_name) | n/a | `string` | `"filebeat-lop-crt"` | no |
| <a name="input_filebeat_namespace"></a> [filebeat\_namespace](#input\_filebeat\_namespace) | n/a | `string` | `"beats"` | no |
| <a name="input_filebeat_repo_url"></a> [filebeat\_repo\_url](#input\_filebeat\_repo\_url) | n/a | `string` | n/a | yes |
| <a name="input_filebeat_taints"></a> [filebeat\_taints](#input\_filebeat\_taints) | n/a | <pre>list(object({<br>    key    = string<br>    value  = string<br>    effect = string<br>  }))</pre> | `[]` | no |
| <a name="input_project_environment"></a> [project\_environment](#input\_project\_environment) | n/a | `string` | n/a | yes |
| <a name="input_tolerations_mapping"></a> [tolerations\_mapping](#input\_tolerations\_mapping) | n/a | `map` | <pre>{<br>  "NO_EXECUTE": "NoExecute",<br>  "NO_SCHEDULE": "NoSchedule",<br>  "PREFER_NO_SCHEDULE": "PreferNoSchedule"<br>}</pre> | no |

## Outputs

No outputs.
<!-- END_TF_DOCS -->