variable "tolerations_mapping" {
  default = {
    "NO_SCHEDULE"        = "NoSchedule",
    "PREFER_NO_SCHEDULE" = "PreferNoSchedule",
    "NO_EXECUTE"         = "NoExecute"
  }
}

locals {
  tolerations = [
    for taint in var.filebeat_taints : {
      key      = taint.key
      value    = taint.value
      effect   = lookup(var.tolerations_mapping, taint.effect)
      operator = "Equal"
    }
  ]
}

resource "kubernetes_secret" "repository" {
  metadata {
    name      = "filebeat-repo"
    namespace = var.argocd_namespace
    labels = {
      "argocd.argoproj.io/secret-type" = "repository"
    }
  }
  data = {
    project = var.argocd_project
    type    = "git"
    url     = var.filebeat_repo_url
  }
  type = "Opaque"
}

resource "time_sleep" "wait_10_seconds" {
  depends_on = [kubernetes_secret.repository]

  create_duration = "10s"
}

resource "kubernetes_manifest" "argocd_application" {
  manifest = yamldecode(
    templatefile("${path.module}/argocd-application.yaml", {
      argocd_name      = "gitlab-runner"
      argocd_namespace = var.argocd_namespace
      argocd_project   = var.argocd_project
      repo             = var.filebeat_repo_url
      namespace        = var.filebeat_namespace
      tolerations      = local.tolerations
      environment      = var.project_environment
      log_paths        = var.filebeat_log_paths
      lop_address      = var.filebeat_lop_address
      lop_secret_name  = var.filebeat_lop_secret_name
      ca_crt           = var.filebeat_ca_crt
      crt              = var.filebeat_crt
      key              = var.filebeat_key
    })
  )
  depends_on = [kubernetes_secret.repository, time_sleep.wait_10_seconds]
}
